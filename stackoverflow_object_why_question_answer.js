// Taken from this Stackoverflow question.
// http://stackoverflow.com/questions/6930601/why-how-should-i-used-objects-in-javascript

function Person(firstName, lastName, gender, age) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.gender = gender;
  this.age = age;
}

Person.prototype = {
  getFullName: function() { return this.firstName + ' ' + this.lastName; },
  isMale: function() { return this.gender == 'Male'; },
  isFemale: function() { return this.gender == 'Female'; }
};

var amanda = new Person('Amanda', 'Smith', "Female", 42);
var john = new Person('John', 'Doe', 'Male', 72);

console.log(amanda.getFullName());
console.log(john.isMale());